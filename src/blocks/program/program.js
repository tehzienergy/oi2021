var itemPositioning = function (program) {

  var programTimeList = program.find('.program__time-wrapper');

  var timeNormal = function (i) {
    var decimalTimeString = i;
    var n = new Date(0, 0);
    n.setMinutes(+decimalTimeString * 60);
    var result = n.toTimeString().slice(0, 5);
    return result;
  }

  var programDayFilling = function () {
    var dayStarts = programTimeList.data('day-start');
    var dayEnds = programTimeList.data('day-end');
    for (var i = dayStarts; i <= dayEnds; i += .5) {
      programTimeList.append('<div class="program__time" data-time="' + i + '"><span>' + timeNormal(i) + '</span></div>')
    }
  }

  programDayFilling();
  var itemPlacement = function () {
    var event = program.find('.event');
    event.each(function () {
      var eventTimeStart = $(this).data('sh') + ($(this).data('sm') / 60);
      var eventTimeEnd = $(this).data('fh') + ($(this).data('fm') / 60);
      var eventDuration = eventTimeEnd - eventTimeStart;
      $(this).css('top', (eventTimeStart - 10) * 392 + 16 + 'px');
      $(this).css('height', (eventTimeEnd - eventTimeStart) * 392 - 16 + 'px');
    });
  }

  itemPlacement();
};


if ($(window).width() > 991) {
  $('.program').each(function () {
    itemPositioning($(this));
  });
}

$('.program__tabs-controls .form-check-input').change(function () {
  $('.program__tab').toggleClass('program__tab--active');
});

if ($(window).width() > 991) {
  $(document).ready(function () {

    // Cache the highest
    var highestBox = 0;

    // Select and loop the elements you want to equalise
    $('.program__col-header', this).each(function () {

      // If this box is higher than the cached highest then store it
      if ($(this).height() > highestBox) {
        highestBox = $(this).height();
      }
    });

    // Set the height of all those children to whichever was highest 
    $('.program__col-header', this).height(highestBox);

  });
}