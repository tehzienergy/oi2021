$('.slider__content--items').each(function() {
  var arrowPrev = $(this).closest('.slider').find('.slider__arrow--prev');
  var arrowNext = $(this).closest('.slider').find('.slider__arrow--next');
  $(this).slick({
    slidesToShow: 3,
    prevArrow: arrowPrev,
    nextArrow: arrowNext,
    infinite: false,
    rows: 0,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          adaptiveHeight: true
        }
      },
    ]
  });
});

$('.slider__content--frontmans').each(function() {
  var arrowPrev = $(this).closest('.slider').find('.slider__arrow--prev');
  var arrowNext = $(this).closest('.slider').find('.slider__arrow--next');
  var dots = $(this).closest('.slider').find('.slider__dots');
  $(this).slick({
    slidesToShow: 4,
    prevArrow: arrowPrev,
    nextArrow: arrowNext,
    dots: true,
    appendDots: dots,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4500,
    speed: 500,
    rows: 0,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          adaptiveHeight: true
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          variableWidth: false,
          autoplay: false
        }
      },
    ]
  });
});

$('.slider__content--histories').each(function() {
  var arrowPrev = $(this).closest('.slider').find('.slider__arrow--prev');
  var arrowNext = $(this).closest('.slider').find('.slider__arrow--next');
  var dots = $(this).closest('.slider').find('.slider__dots');
  $(this).slick({
    slidesToShow: 4,
    prevArrow: arrowPrev,
    nextArrow: arrowNext,
    dots: true, 
    appendDots: dots,
//    autoplay: true,
//    autoplaySpeed: 4500,
    speed: 500,
    rows: 0,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          adaptiveHeight: true
        }
      },
    ]
  });
});
