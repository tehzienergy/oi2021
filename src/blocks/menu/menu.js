$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').toggleClass('menu__content--active');
  $('body').toggleClass('fixed');
});


if ($(window).width() < 992) {
  $('.menu__link--submenu').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.menu__item').toggleClass('menu__item--submenu');
  });
}