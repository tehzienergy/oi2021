if ($('.scrollbar--x').length) {

  $('.scrollbar--x').each(function() {
    const ps2 = new PerfectScrollbar(this, {
      suppressScrollY: true,
      useBothWheelAxes: true,
    });  
  })
}
