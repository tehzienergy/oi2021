$('.article__chat-btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('article__chat-btn--active');
  $(this).closest('.article__chat').find('.article__chat-content').slideToggle('fast');
})
