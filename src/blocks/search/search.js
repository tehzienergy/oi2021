$('.search__input').on('input', function() {
  if ($(this).val() != '') {
    $('.search__clear').fadeIn('fast');
    $('.search').addClass('search--active');
    
    /* Для демонстрации поиска */
    $('.program__tabs').hide();
    $('.program__search-results').fadeIn('fast');
  }
  else {
    $('.search__clear').fadeOut('fast');
    $('.search').removeClass('search--active');
    
    /* Для демонстрации поиска */
    $('.program__tabs').fadeIn('fast');
    $('.program__search-results').hide();
  }
});

$('.search__clear').click(function(e) {
  e.preventDefault();
  $(this).closest('.search').find('.search__input').val('');
  $('.search__clear').fadeOut('fast');
  $('.search').removeClass('search--active');
    
    /* Для демонстрации поиска */
    $('.program__tabs').fadeIn('fast');
    $('.program__search-results').hide();
});

