$('.faq__header').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('faq__header--active');
  $(this).closest('.faq').find('.faq__content').slideToggle('fast');
})